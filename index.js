const express = require('express');
const app = express();
const PORT = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/home", (req, res) => res.send(`Welcome to the home page`));

let users=[{
    username: "johndoe",
    password: "johndoe1234"
}];

app.get("/users", (req, res) => {
    res.send(JSON.stringify(users));
});

app.delete("/delete-user", (req, res) => {
    console.log(users)

    for (let i = 0; i < users.length; i++) {
        if (users[i].username == req.body.username) {
            delete users[i].username
            res.send(`User ${req.body.username} has been deleted.`)
        } else {
            res.send(`User ${req.body.username} doesn't exist`);
        }
        console.log(users)
    }
})


app.listen(PORT, () => console.log(`Server running at port ${PORT}`));